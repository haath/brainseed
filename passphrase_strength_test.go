package main

import (
	"testing"
)

func TestGetPassphraseStrength(t *testing.T) {

	cases := []struct {
		passphrase    string
		expectedScore int
	}{
		{"123", 0},
		{"correcthorse123", 1},
		{"!@#correcthorse123", 2},
		{"!@#correcthorse 123", 3},
		{"!@#correcthorse123 _=1", 4},
	}

	for _, tc := range cases {

		strength, _ := GetPassphraseStrength(tc.passphrase, "")
		actual := strength.Score

		if actual != tc.expectedScore {

			t.Fatalf("expected: %d got: %d, for %s", tc.expectedScore, actual, tc.passphrase)
		}
	}
}

func TestPrintSummary(t *testing.T) {

	// only for coverage
	var strength PassphraseStrength

	strength.PrintSummary()
}
