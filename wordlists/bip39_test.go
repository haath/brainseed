package wordlists

import (
	"strings"
	"testing"
)

func TestBip39GetChecksumOptions(t *testing.T) {

	bip39 := bip39WordList{}

	cases := []struct {
		phrase   string
		expected []string
	}{
		{"laugh alpha lunar someone enemy skull domain brand act inspire cage exit eyebrow quick bacon guess teach smile winter click often gasp ill",
			[]string{
				"among", "chunk", "dove", "jungle", "mammal", "rice", "stove", "visa",
			},
		},
		{"proud radio job bring follow slab parent zero idea silent silly ready fun salmon",
			[]string{
				"able", "alley", "area", "arrange", "banner", "bind", "borrow", "broken", "call", "cattle", "citizen", "confirm", "country", "dash", "depart", "dish", "draft", "dutch", "empty", "exact", "faint", "fetch", "fortune", "fragile", "gate", "grief", "head", "hungry", "imitate", "isolate", "kick", "latin", "logic", "loyal", "mercy", "moon", "myself", "nest", "observe", "orchard", "people", "picnic", "predict", "quick", "ramp", "renew", "roast", "sample", "scare", "session", "slim", "slush", "spirit", "stamp", "survey", "symptom", "throw", "tone", "trouble", "upset", "vessel", "want", "way", "wolf",
			},
		},
	}

	for _, tc := range cases {

		checksumOptions := bip39.GetChecksumOptions(strings.Split(tc.phrase, " "))
		expected := tc.expected

		if len(checksumOptions) != len(expected) {
			t.Fatalf("expected: %v\ngot: %v", len(expected), len(checksumOptions))
		}

		for i := 0; i < len(expected); i++ {

			if checksumOptions[i] != expected[i] {
				t.Fatalf("expected: %v\ngot: %v", checksumOptions[i], expected[i])
			}
		}
	}
}
