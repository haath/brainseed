package wordlists

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type customWordList struct {
	words []string
}

func (wordList *customWordList) GetWords() []string {

	return wordList.words
}

func (wordList *customWordList) GetChecksumOptions(phrase []string) []string {

	return wordList.words
}

func (wordList *customWordList) PhraseLengthValid(length int) (bool, error) {

	return true, nil
}

func (wordList *customWordList) DefaultLength() int {

	return 12
}

func loadWordListFromFile(path string) (WordList, error) {

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, fmt.Errorf("Wordlist provided is not one of the available options and does not point to a file.\nAvailable options are: monero, bip39")
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var words []string
	for scanner.Scan() {

		line := scanner.Text()
		word := strings.TrimSpace(line)

		if word == "" {
			continue
		}

		if strings.Contains(word, " ") {

			return nil, fmt.Errorf("Each line in the given wordlist file needs to contain only one word.")
		}

		words = append(words, word)
	}

	wordList := customWordList{
		words: words,
	}

	return &wordList, nil
}
