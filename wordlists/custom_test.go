package wordlists

import (
	"testing"
)

func TestCustomWordListNotExists(t *testing.T) {

	_, err := GetWordList("yeet")

	if err == nil {
		t.Fatalf("expected error")
	}
}

func TestCustomWordList(t *testing.T) {

	wordList, err := GetWordList("../assets/test_wordlist_correct.txt")

	if err != nil {
		t.Fatalf("got error: %v", err)
	}

	expected := []string{"this", "is", "a", "custom", "word", "list", "for", "unit", "tests"}

	words := wordList.GetWords()
	checksumOptions := wordList.GetChecksumOptions([]string{})
	valid, err := wordList.PhraseLengthValid(1)
	if err != nil || !valid {
		t.Fatalf("got error: %v", err)
	}

	for i := 0; i < len(expected); i++ {

		if words[i] != expected[i] {
			t.Fatalf("expected: %v\ngot: %v", words[i], expected[i])
		}
		if checksumOptions[i] != expected[i] {
			t.Fatalf("expected: %v\ngot: %v", checksumOptions[i], expected[i])
		}
	}
}

func TestCustomWordListWrong(t *testing.T) {

	_, err := GetWordList("../assets/test_wordlist_wrong.txt")
	if err == nil {
		t.Fatalf("expected error")
	}
}
