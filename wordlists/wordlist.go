package wordlists

import (
	"strings"
)

type WordList interface {
	// Returns the wordlist as a string array of words.
	GetWords() []string
	// Computes and returns a list of words, which are valid as the final checksum word to complete the current phrase.
	GetChecksumOptions(phrase []string) []string
	// Returns true if the given length is valid for recovery phrases of this type, or false and an error if it is not valid.
	PhraseLengthValid(length int) (bool, error)
	// Returns the default seed phrase length for the wordlist.
	DefaultLength() int
}

func GetWordList(name string) (WordList, error) {

	switch strings.ToLower(name) {
	case "xmr", "monero":
		return &moneroWordList{}, nil
	case "dice", "diceware":
		return &dicewareWordList{}, nil
	case "bip39", "metamask", "":
		return &bip39WordList{}, nil
	default:
		return loadWordListFromFile(name)
	}
}
