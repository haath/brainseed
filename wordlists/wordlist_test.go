package wordlists

import "testing"

func TestValidLength(t *testing.T) {

	cases := []struct {
		wordList string
		length   int
		expected bool
	}{
		{"bip39", 12, true},
		{"bip39", 13, false},
		{"monero", 25, true},
		{"monero", 15, false},
		{"dice", 99, true},
	}

	for _, tc := range cases {

		wordList, _ := GetWordList(tc.wordList)
		valid, _ := wordList.PhraseLengthValid(tc.length)

		if valid != tc.expected {

			t.Fatalf("expect %v %v %v, got %v", tc.wordList, tc.length, tc.expected, valid)
		}
	}
}

func TestDefaultLengthValid(t *testing.T) {

	wordListNames := []string{"bip39", "monero", "dice", "../assets/test_wordlist_correct.txt"}

	for _, wordListName := range wordListNames {

		wordList, err := GetWordList(wordListName)

		if err != nil {
			t.Fatalf("got error: %v", err)
		}

		defaultLength := wordList.DefaultLength()

		if valid, _ := wordList.PhraseLengthValid(defaultLength); !valid {
			t.Fatalf("default length %v for %v is not valid", defaultLength, wordListName)
		}
	}
}

func TestInvalidWordlist(t *testing.T) {

	wordList, err := GetWordList("yeet")

	if wordList != nil || err == nil {

		t.Fatalf("expect error, got %v %v", wordList, err)
	}
}
