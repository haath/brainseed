package wordlists

import (
	"strings"
	"testing"
)

func TestMoneroGetChecksumOptions(t *testing.T) {

	xmr := moneroWordList{}
	xmr.GetWords() //coverage

	cases := []struct {
		phrase   string
		expected []string
	}{
		{"skirting trash phase buckets apology gags sedan coffee vinegar else fifteen pitched idled gorilla siren cucumber urban junk vastness laboratory rift rhino situated taxi",
			[]string{
				"buckets",
			},
		},
		{"zones waking opacity auburn match hookup ramped huts fuming broken strained sedan duplex satin arsenic leisure huge apology hydrogen uttered reorder aplomb enjoy error",
			[]string{
				"error",
			},
		},
		{"rudely oscar spiders insult isolated pitched object begun cuisine bias lakes wield",
			[]string{
				"oscar",
			},
		},
	}

	for _, tc := range cases {

		checksumOptions := xmr.GetChecksumOptions(strings.Split(tc.phrase, " "))
		expected := tc.expected

		if len(checksumOptions) != len(expected) {
			t.Fatalf("expected: %v\ngot: %v", len(expected), len(checksumOptions))
		}

		for i := 0; i < len(expected); i++ {

			if checksumOptions[i] != expected[i] {
				t.Fatalf("expected: %v\ngot: %v", checksumOptions[i], expected[i])
			}
		}
	}
}
