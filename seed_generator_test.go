package main

import (
	"brainseed/wordlists"
	"strings"
	"testing"
)

func TestGetSecret(t *testing.T) {

	gen := seedGenerator{
		passphrase: "foo",
		length:     123,
		salt:       "bar",
	}

	secret := gen.getSecret()
	expected := "foo;123;bar"

	if secret != expected {

		t.Fatalf("expect %v, got %v", expected, secret)
	}
}

func TestGenerate(t *testing.T) {

	wordListNames := []string{"bip39", "monero", "diceware"}

	for _, wordListName := range wordListNames {

		wordList, _ := wordlists.GetWordList(wordListName)
		gen := NewSeedGenerator(wordList, 12, "passphrase", "salt")
		phrase := gen.Generate()

		words := strings.Split(phrase, " ")

		if len(words) != 12 {

			t.Fatalf("expected length 12, got %v", len(words))
		}
	}
}

func TestGenerateDeterministic(t *testing.T) {

	wordList, _ := wordlists.GetWordList("bip39")
	gen1 := NewSeedGenerator(wordList, 12, "passphrase", "salt")
	phrase1 := gen1.Generate()

	gen2 := NewSeedGenerator(wordList, 12, "passphrase", "salt")
	phrase2 := gen2.Generate()

	if phrase1 != phrase2 {

		t.Fatalf("expect: %v\nequal to: %v", phrase1, phrase2)
	}
}
