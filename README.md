<div align="center">
    <img src="assets/logo.png" /><br/>
    <a href="https://gitlab.com/haath/brainseed/pipelines"><img src="https://gitlab.com/haath/brainseed/badges/master/pipeline.svg" /></a>
    <a href="https://gitlab.com/haath/brainseed/-/jobs/artifacts/master/browse?job=test"><img src="https://gitlab.com/haath/brainseed/badges/master/coverage.svg" /></a>
    <a href="https://gitlab.com/haath/brainseed/blob/master/LICENSE"><img src="https://img.shields.io/badge/license-MIT-blue.svg?style=flat" /></a>
</div>

---

This tool deterministically produces recovery seed phrases for cryptocurrency wallets, that are derived from a given string.

- Store wallets in memory, without memorizing long seed phrases.
- Can produce a variable number of words, so it is compatible with any wallet and cryptocurrency.
- Phrases are generated from as much entropy as is given in the input passphrase, up to a maximum of 256 bits.
- Generation algorithm is simple, should be easy to reproduce even if this project has been deleted in the future.
- Passphrase is opt-out checked with [zxcvbn](https://github.com/nbutton23/zxcvbn-go) to somewhat prevent the creation of guessable wallets.
- Optional providing of personal salt (like e-mail) to guard against wide-net guessing.

Any custom wordlist can be provided in a text file using the `--wordlist` option.
However, checksum-valid seed phrases can be generated for the available built-in wordlists.
Those are currently the following:

- BIP39
- Monero


## Usage

Build from source using `go build` (recommended), or download precompiled binaries for the latest version from the [releases page](https://gitlab.com/haath/brainseed/-/releases) which are automatically built by GitLab CI.

Then just execute on the command line, there will be a prompt to enter a passphrase.

```shell
$ brainseed
Enter passphrase: correct horse battery staple
galaxy test dinner diesel guitar trap cabbage forward mention badge nest powder
```

Optionally it is also possible to specify a salt (recommended) or a different seed phrase length to use with a different wallet. Monero wallets for example use 25 words.

```shell
$ brainseed -l 14 -s some_salt
Enter passphrase: correct horse battery staple
warrior gorilla frequent supply daring element green record forget film flight steel must toilet
```


## Why?

Cryptocurrency wallets need to have their recovery seed phrase stored securely.
Storing on a physical medium like paper comes with its own set of problems, because what if all copies are somehow damaged?
Or what if a copy of the recovery seed is stolen? On the other hand if one tries to obfuscate the seed to protect against theft, how is it ensured that they will remember the obfuscation method when they suddenly need to restore the wallet from their seed phrase 10 years later?

This creates the need for a so-called "brain wallet", which is a recovery seed that one can commit to memory.
For the average person, it is impractical and risky to try to memorize the phrase which might be 12 or 24 words long.
As it turns out though, due to muscle memory and other factors, humans are generally much better at memorizing passwords.

Apart from convenience and security, there are cases where a brain wallet might even be necessary:

- **Plausible deniability**: Storing a wallet on a hard drive or other physical medium, even if encrypted, can betray the wallet's existence.
- **Transport of funds**: Crossing a dangerous border or living under a totalitarian regime, one can expect to be physically searched often. A brain wallet could be a great way to safely transport large amounts of money on your person, without fear that it will be found.


## How?

The generation process is the following.

1. Read from user input: the `passphrase`, `salt` and number of words as `length`.
2. Produce combined semicolon-separated string `passphrase;length;salt` as `secret`.
3. Compute SHA256 hash `pHash = sha256(secret)`.
4. Loop ten million times:
    - Compute SHA256 hash `pHash = sha256(pHash)`.
5. Loop for number of words we need to generate (except the last):
    1. Take first big-endian `uint64` from `pHash` as `s`.
    2. Compute `idx = s % 2048`.
    3. Pick word from the `BIP39 wordlist` at index `idx`.
    4. Compute next SHA256 hash `pHash = sha256(pHash)`.
6. For the last word:
    1. Get all possible last words that would have a valid checksum as `checksumOptions`.
    2. Take first big-endian `uint64` from `pHash` as `s`.
    3. Compute `idx = s % len(checksumOptions)`.
    4. Pick word from `checksumOptions` at index `idx`.


Looping ten million times over the hashing of `secret` is meant to increase execution time, so that an attacker who knows this generation process is still limited in terms of how many guesses he can perform per second.


## Is it secure?

Generally brain wallets are considered insecure, see [here](https://bitcointalk.org/index.php?topic=1148611.0) and [here](https://www.youtube.com/watch?v=foil0hzl4Pg).

But the insecurity mostly comes from the passphrase selection, since most people are generally terrible at picking good passwords.
Assuming that someone knows the derivation method (this tool), using a 5-character passphrase will get your funds stolen immediately by active attackers.
Using 6 characters or a bit more may still get you brute-forced in a trivial amount of time, especially if your passphrase is stupid and includes known phrases or song lyrics.

But assuming that the passphrase is long and sufficiently secure, it could have a decent 100 bits of entropy.
This is roughly the same amount of entropy that you'd get via [the Metamask wallet recovery phrase](https://ethereum.stackexchange.com/a/94785) for example, which is 12 words selected from a list of 2048 words.

Generally speaking, a brain wallet is as secure as a password. This sounds good, right? Except it's not.
It is important to recognize that, password guessing can be blocked by network throttling and captchas.
What's more, passwords often protect inconsequential accounts, which are even usually protected by 2FA.
To guess a brain wallet though, an attacker can operate offline, with infinite attempts that are only limited by computing power, and with the prize being instantaneous monetary gain.

This tool could be secure to use if:

- The passphrase is strong and hard to guess.
- A personal salt is provided (like an e-mail), so even a guessing attacker would have to target you specifically.
- Weigh the risk and benefit of using such a "brain wallet" method against your own situation.
- You understand exactly how the seed is produced, so that you may not rely on this tool in the future.


## Versioning

This project will be using the [semantic versioning](https://semver.org/) scheme to denote its [releases](https://gitlab.com/haath/brainseed/-/tags).
This means that version numbers will be in the format `Major.Minor.Patch`.
It is important to check the version of the application when generating a seed phrase using the `--version` flag.

Updates will be rolled out in the following way:

- The `Patch` number will be changed when there is a fix or improvement that does not affect the usage of the application in any way.
- The `Minor` number will be changed when there is a change to the interface of the application, for example if the command-line parameters are updated or if there is a change in the format in which data are printed out.
- The `Major` number will be changed when there is any update in the seed generation algorithm. This means that seed phrases generated using this application, can only be restored in the future by using the same major version. If there is a change in the major version, it will only be because a serious flaw or vulnerability was discovered in the algorithm. In such a case, it is recommended to create a new wallet (with a different passphrase if using this tool), and move funds out of the brain wallets that were generated using the older major version.

