package main

import (
	"testing"
)

func TestParse(t *testing.T) {

	var opts options

	err := opts.parse()

	if err == nil {

		t.Fatalf("expected error due to test flags")
	}
}

func TestValidate(t *testing.T) {

	// these test cases are mostly for coverage
	var opts options
	opts.Passphrase = "@#^fgbsd.--123=sd"
	opts.Report = true

	_, err := opts.validate()
	if err != nil {
		t.Fatalf("got error: %v", err)
	}

	opts.Passphrase = ""
	_, err = opts.validate()
	if err == nil {
		t.Fatalf("expected error")
	}
}
