package main

import (
	"brainseed/wordlists"
	"bufio"
	"fmt"
	"os"

	"github.com/jessevdk/go-flags"
)

var Options options

type options struct {
	WordList   string `short:"w" long:"wordlist" description:"The wordlist to use. Can be one of built-in options, or a path to a text file.\nAvailable options are: bip39, monero\n" default:"bip39"`
	Length     uint   `short:"l" long:"length" description:"The length of the seed phrase, in number of words.\nThe default value depends on the selected wordlist.\n- BIP39: 12\n- Monero: 25\n- Other: 12"`
	Passphrase string `short:"p" long:"passphrase" description:"The passphrase to generate the seed from.\nLeave black to enter it from stdin instead."`
	Salt       string `short:"s" long:"salt" description:"Optional salt to combine with the passphrase for the seed generation. It is recommended to set this to something like an e-mail address."`
	Report     bool   `short:"r" long:"report" description:"Print out a report of the passphrase strength using zxcvbn."`
	Unsafe     bool   `long:"unsafe" description:"Allow the generation of a seed from a weak passphrase."`
	Version    bool   `short:"v" long:"version" description:"Print the current version and exit."`
}

func (opts *options) parse() error {

	parser := flags.NewParser(opts, flags.HelpFlag|flags.PassDoubleDash|flags.PrintErrors)

	_, err := parser.Parse()
	return err
}

func (opts *options) validate() (wordlists.WordList, error) {

	// get user passphrase from stdin
	if opts.Passphrase == "" {
		fmt.Print("Enter passphrase: ")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		opts.Passphrase = scanner.Text()
	}

	// compute passphrase strength
	strength, err := GetPassphraseStrength(opts.Passphrase, opts.Salt)
	if err != nil {
		return nil, err
	}

	// prevent the user from generating seeds from weak passphrases
	if !opts.Unsafe && strength.Score < 4 {

		strength.PrintSummary()
		return nil, fmt.Errorf("Passphrase is too weak, a score of 4 is required. (to bypass this use --unsafe)")
	}

	// initialize wordlist and check if it's valid
	wordList, err := wordlists.GetWordList(opts.WordList)
	if err != nil {
		return nil, fmt.Errorf("%v\n", err)
	}

	// if no length is provided, get the default for the selected wordlist
	if opts.Length == 0 {
		opts.Length = uint(wordList.DefaultLength())
	}

	// check if the length is valid for this wordlist
	if _, err = wordList.PhraseLengthValid(int(opts.Length)); err != nil {
		return nil, fmt.Errorf("%v\n", err)
	}

	// print out a strength report if user requested it
	if opts.Report {

		strength.PrintSummary()
	}
	return wordList, nil
}
