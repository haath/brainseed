package util

import (
	"encoding/binary"
	"testing"
)

func TestGetFirstUint32(t *testing.T) {

	var buf [8]byte
	binary.BigEndian.PutUint32(buf[:], 0xbeef)

	ret := GetFirstUint32(buf[:])
	if ret != 0xbeef {
		t.Fatalf("expect %v, got %v", 0xbeef, ret)
	}
}

func TestGetFirstUint64(t *testing.T) {

	var buf [8]byte
	binary.BigEndian.PutUint64(buf[:], 0xbeef)

	ret := GetFirstUint64(buf[:])
	if ret != 0xbeef {
		t.Fatalf("expect %v, got %v", 0xbeef, ret)
	}
}

func TestPadLeft(t *testing.T) {

	var buf [8]byte

	padded := PadLeft(buf[:], 12)

	if len(padded) != 12 {
		t.Fatalf("expect %v, got %v", 12, len(padded))
	}
}

func TestIndexOf(t *testing.T) {

	arr := []string{"one", "two", "three", "four"}

	indTwo := IndexOf(arr, "two")
	indFour := IndexOf(arr, "four")
	indFive := IndexOf(arr, "five")

	if indTwo != 1 {
		t.Fatalf("expected %v, got %v", 1, indTwo)
	}
	if indFour != 3 {
		t.Fatalf("expected %v, got %v", 3, indFour)
	}
	if indFive != -1 {
		t.Fatalf("expected %v, got %v", -1, indFive)
	}
}

func TestContainsInt(t *testing.T) {

	arr := []int{1, 2, 3, 4}

	indTwo := ContainsInt(arr, 1)
	indFour := ContainsInt(arr, 4)
	indFive := ContainsInt(arr, 5)

	if indTwo != true {
		t.Fatalf("expected %v, got %v", true, indTwo)
	}
	if indFour != true {
		t.Fatalf("expected %v, got %v", true, indFour)
	}
	if indFive != false {
		t.Fatalf("expected %v, got %v", false, indFive)
	}
}
