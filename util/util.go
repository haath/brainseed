package util

import (
	"bytes"
	"encoding/binary"
)

// GetFirstUint32 returns the first 4 bytes in the given buffer as a uint32.
func GetFirstUint32(buf []byte) uint32 {

	bufReader := bytes.NewBuffer(buf)

	var val uint32
	binary.Read(bufReader, binary.BigEndian, &val)

	return val
}

// GetFirstUint32 returns the first 8 bytes in the given buffer as a uint64.
func GetFirstUint64(buf []byte) uint64 {

	bufReader := bytes.NewBuffer(buf)

	var val uint64
	binary.Read(bufReader, binary.BigEndian, &val)

	return val
}

// PadLeft prepends zeroes to the given buffer, until the given target length is reached.
func PadLeft(buf []byte, length int) []byte {

	padding := make([]byte, length-len(buf))
	return append(padding, buf...)
}

// Returns the index of a given string in the given string array, or -1 if the string is not present.
func IndexOf(arr []string, key string) int {
	for i, v := range arr {
		if v == key {
			return i
		}
	}
	return -1
}

// Returns true if the given integer array contains the given key.
func ContainsInt(arr []int, key int) bool {
	for _, v := range arr {
		if v == key {
			return true
		}
	}
	return false
}
