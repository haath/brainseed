package main

import (
	"brainseed/util"
	"brainseed/wordlists"
	"crypto/sha256"
	"fmt"
	"strings"
)

type seedGenerator struct {
	wordList   wordlists.WordList
	length     int
	passphrase string
	salt       string
}

type SeedGenerator interface {
	Generate() string
}

func NewSeedGenerator(wordList wordlists.WordList, length uint, passphrase string, salt string) SeedGenerator {

	return &seedGenerator{
		wordList:   wordList,
		length:     int(length),
		passphrase: passphrase,
		salt:       salt,
	}
}

func (gen *seedGenerator) Generate() string {

	// pick a wordlist
	wordList := gen.wordList
	wordListWords := wordList.GetWords()

	// get combined secret from passphrase, salt, and length
	secret := gen.getSecret()

	// hash secret with sha256
	pHash := sha256.Sum256([]byte(secret))

	// repeat hash process ten million times to pad execution speed
	for i := 0; i < 10e6; i++ {
		pHash = sha256.Sum256(pHash[:])
	}

	var phrase []string

	// generate the phrase, except for the last word which needs to be a checksum word
	for i := 0; i < gen.length-1; i++ {

		// map the current hash to a word in the wordlist and add it
		word := hashToWord(wordListWords, pHash[:])
		phrase = append(phrase, word)

		// advance to the next hash
		pHash = sha256.Sum256(pHash[:])
	}

	// add checksum word to the end
	checkSumWordOptions := wordList.GetChecksumOptions(phrase)
	checksumWord := hashToWord(checkSumWordOptions, pHash[:])
	phrase = append(phrase, checksumWord)

	return strings.Join(phrase, " ")
}

func (gen *seedGenerator) getSecret() string {

	return fmt.Sprintf("%s;%d;%s", gen.passphrase, gen.length, gen.salt)
}

// Shortcut for getting a word from a wordlist at a position specified by the first uint64 in a given hash.
func hashToWord(wordList []string, hash []byte) string {

	s := util.GetFirstUint64(hash)
	idx := s % uint64(len(wordList))
	word := wordList[idx]
	return word
}
