package main

import (
	"fmt"

	"github.com/nbutton23/zxcvbn-go"
)

type PassphraseStrength struct {
	Score            int     `json:"score"`
	ScoreDescription string  `json:"score_description"`
	Entropy          int     `json:"entropy"`
	CrackTime        float64 `json:"crack_time"`
	CrackTimeDisplay string  `json:"crack_time_display"`
}

func GetPassphraseStrength(passphrase string, salt string) (*PassphraseStrength, error) {

	dropboxStrength := zxcvbn.PasswordStrength(passphrase, []string{salt})

	var scoreDescription string
	switch dropboxStrength.Score {
	case 0:
		scoreDescription = "too guessable, risky password"
	case 1:
		scoreDescription = "very guessable, protection from throttled online attacks"
	case 2:
		scoreDescription = "somewhat guessable, protection from unthrottled online attacks"
	case 3:
		scoreDescription = "safely unguessable, moderate protection from offline slow-hash scenario"
	case 4:
		scoreDescription = "very unguessable, strong protection from offline slow-hash scenario"
	default:
		return nil, fmt.Errorf("invalid score from zxcvbn: %d\n", dropboxStrength.Score)
	}

	strength := PassphraseStrength{
		Score:            dropboxStrength.Score,
		ScoreDescription: scoreDescription,
		Entropy:          int(dropboxStrength.Entropy),
		CrackTime:        dropboxStrength.CrackTime,
		CrackTimeDisplay: dropboxStrength.CrackTimeDisplay,
	}
	return &strength, nil
}

func (strength *PassphraseStrength) PrintSummary() {

	fmt.Printf("Score: %d\n", strength.Score)
	fmt.Printf("Description: %s\n", strength.ScoreDescription)
	fmt.Printf("Entropy: %d bits\n", strength.Entropy)
	fmt.Printf("Crack time: %s\n", strength.CrackTimeDisplay)
	fmt.Printf("\n")
}
