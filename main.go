package main

import (
	"fmt"
	"os"
)

var Version = "v0.0.0"

func main() {

	err := Options.parse()
	if err != nil {
		os.Exit(1)
	}

	if Options.Version {
		fmt.Println(Version)
		os.Exit(0)
	}

	wordList, err := Options.validate()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(2)
	}

	generator := NewSeedGenerator(wordList, Options.Length, Options.Passphrase, Options.Salt)
	seedPhrase := generator.Generate()

	fmt.Println(seedPhrase)
}
